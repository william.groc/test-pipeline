import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.test.android',
  appName: 'AppTest',
  webDir: 'www',
  server: {
    androidScheme: 'http',
    allowNavigation: ['*'],
    hostname: 'localhost'
  },
  plugins: {
    PushNotifications: {
      presentationOptions: ["badge", "sound", "alert"]
    }
  }
};

export default config;
