# Build and send to firebase

## Configuration

Download and copy firebase_login_credential.json into build_ressources/android/fastlane
Download and copy google-services.json into build_ressources/android/app

Update fastlane/env.json

## Run

### Android

Build ionic
```sh
npm run build:android
```

Build ionic & build apk & send to firebase
```sh
npm run release:android
```