import { HttpClient } from "@angular/common/http";
import { Injectable, NgZone } from "@angular/core";
import { App } from "@capacitor/app";
import { Browser } from "@capacitor/browser";
import { authCodeFlowConfig } from "./config";
import { AccessTokenDecoded, SsoToken } from "./model";
import { generateNonce, base64URLEncode, sha256 } from "./utils";
import jwt_decode from 'jwt-decode';

@Injectable({ providedIn: 'root' })
export class AuthService {
  access_token = '';
  id_token = '';
  refresh_token = '';

  constructor(private readonly http: HttpClient, private readonly zone: NgZone) {
  }

  private buildLoginUrl(codeChallenge: string): string {
    return authCodeFlowConfig.issuer
      + '/protocol/openid-connect/auth?'
      + `client_id=${encodeURIComponent(authCodeFlowConfig.client_id)}`
      + `&response_type=${authCodeFlowConfig.response_type}`
      + `&scope=${authCodeFlowConfig.scope}`
      + `&code_challenge_method=S256`
      + `&code_challenge=${codeChallenge}`
      + `&redirect_uri=${authCodeFlowConfig.redirectUri}`;
  }

  private async buildPkce() {
    const codeVerifier = await generateNonce();
    const codeChallenge = base64URLEncode(await sha256(codeVerifier));
    return {
      codeVerifier,
      codeChallenge
    }
  }

  private async login() {
    App.addListener('appUrlOpen', (data: { url: string }) => {
      this.zone.run(async () => {
        const params = new URLSearchParams(data.url.substring(data.url.indexOf('?') + 1));
        console.log('redirected')
        console.log();
        this.access_token = params.get('access_token') || '';

        const code = params.get('code');
        console.log(data.url.substring(data.url.indexOf('?') + 1));
        console.log('code', code);
        if (code) {
          await this.exchangeCodeForToken(code, codeVerifier);
        }
      });
    });

    const { codeVerifier, codeChallenge } = await this.buildPkce();
    await Browser.open({ url: this.buildLoginUrl(codeChallenge) });
  }

  private async refreshSsoToken() {
    if (this.access_token) {
      const newSsoToken = await this.http.post<SsoToken>(`${authCodeFlowConfig.issuer}/oauth2/token`,
        Object.entries({
          "grant_type": "refresh_token",
          "client_id": encodeURIComponent(authCodeFlowConfig.client_id),
          "refresh_token": this.refresh_token,
        }).map(([k, v]) => `${k}=${v}`).join("&"),
        {
          headers: {
            'content-type': 'application/x-www-form-urlencoded',
            'Authorization': `Basic ${authCodeFlowConfig.authorizationBasic}`
          }
        }
      ).toPromise();

      if (newSsoToken)
        this.access_token = newSsoToken.access_token;


      await this.initializeAuthenticationFlow();
    }
  }

  async initializeAuthenticationFlow() {
    if (!this.hasSsoRefreshTokenExpired()) {
      if (!this.hasSsoAccessTokenExpired()) {
        //Add timeout for refresh 1min before expire
        const decodedSSOToken = jwt_decode<AccessTokenDecoded>(this.access_token);
        setTimeout(() => this.refreshSsoToken(), (decodedSSOToken.exp - 60) * 1000 - Date.now());

      } else {
        //Refresh access token
        await this.refreshSsoToken();
      }
    }
    else {
      // Relaunch login process
      await this.login();
      await this.initializeAuthenticationFlow();
    }
  }

  hasSsoAccessTokenExpired(): boolean {
    if (this.access_token) {
      const decryptedAccessToken = jwt_decode<AccessTokenDecoded>(this.access_token);
      const currentDate = Date.now();
      return decryptedAccessToken.exp >= currentDate;
    }
    return true;
  }

  hasSsoRefreshTokenExpired(): boolean {
    if (this.refresh_token) {
      // const decryptedRefreshToken = jwt_decode(ssoToken.refresh_token);
      // const currentDate = Date.now();
      return false;
    }
    return true;
  }

  private async exchangeCodeForToken(code: string, codeVerifier: string): Promise<void> {
    const tokenEndpoint = `${authCodeFlowConfig.issuer}/protocol/openid-connect/token`;
    const body = new URLSearchParams();

    body.set('grant_type', 'authorization_code');
    body.set('client_id', authCodeFlowConfig.client_id);
    body.set('client_secret', 'xrB3GSIPq63tieB9orAICeo2UmyAY0To');
    body.set('code', code);
    body.set('redirect_uri', authCodeFlowConfig.redirectUri);
    body.set('code_verifier', codeVerifier);

    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': `Basic ${authCodeFlowConfig.authorizationBasic}`
    };

    console.log(body.toString())

    try {
      const res = await this.http.post<any>(tokenEndpoint, body.toString(), { headers }).toPromise()
      console.log(res.access_token);

      this.access_token = res.access_token;
      this.refresh_token = res.refresh_token;
      this.id_token = res.id_token;
    } catch (err) {
      console.log('REQUEST ERROR')
      console.log(JSON.stringify(err));
    }
  }
}