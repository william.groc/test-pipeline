import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';

import { AuthService } from './auth.service';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {


  //---------------------------------------------
  //           CONSTRUCTOR
  //---------------------------------------------

  constructor(public authService: AuthService) { }



  //---------------------------------------------
  //           FUNCTIONS
  //---------------------------------------------


  /**
   * Intercept tokens for each HTTP request.
   * @param  {HttpRequest<any>} request
   * @param  {HttpHandler} next
   * @returns {Observable}
   */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const ssotoken = this.authService.access_token

    const headers: Record<string, string> = {
      'Content-Type': 'application/json'
    }

    if (ssotoken) {
      headers['Authorization'] = `Bearer ${ssotoken}`

      request = request.clone({
        setHeaders: headers
      });
    }

    return next.handle(request);
  }
}