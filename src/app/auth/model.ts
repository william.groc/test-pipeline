
export type SsoToken = {
  id_token: string,
  access_token: string,
  refresh_token: string,
  expires_in: number,
}

export type AccessTokenDecoded = {
  exp: number
}