export const back_url = '192.168.1.19'

export const authCodeFlowConfig = {
  // Url of the Identity Provider
  issuer: `http://${back_url}:3000/auth/realms/demo`,

  // URL of the SPA to redirect the user to after login
  redirectUri: 'com.test.android:/',

  // The SPA's id. The SPA is registerd with this id at the auth-server
  // clientId: 'server.code',
  client_id: 'myclient',

  // Just needed if your auth server demands a secret. In general, this
  // is a sign that the auth server is not configured with SPAs in mind
  // and it might not enforce further best practices vital for security
  // such applications.
  // dummyClientSecret: 'secret',

  response_type: 'code',

  // set the scope for the permissions the client should request
  // The first four are defined by OIDC.
  // Important: Request offline_access to get a refresh token
  // The api scope is a usecase specific one
  scope: 'openid',

  authorizationBasic: 'bXljbGllbnQ6eHJCM0dTSVBxNjN0aWVCOW9yQUlDZW8yVW15QVkwVG8='
}